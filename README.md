**Mes modifications :**

// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement 
* pas de booleen, je force l'utilisateur à utiliser un entier car avec booleen ça bug quand c'est inférieur ou supérieur
* pour incrémenter j'utilise addq pour faire +1
* pour décrémenter j'utilise subq pour faire -1


**This version Can handle :**

* // Program := [DeclarationPart] StatementPart
* // DeclarationPart := "[" Identifier {"," Identifier} "]"
* // StatementPart := Statement {";" Statement} "."
* // Statement := AssignementStatement
* // AssignementStatement := Identifier ":=" Expression
* // DisplayStatement := "DISPLAY" Expression
* // WhileStatement := "WHILE" Expression "DO" Statement
* // BlockStatement := "BEGIN" Statement {";" Statement} "END"
* // IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
* // Expression := SimpleExpression [RelationalOperator SimpleExpression]
* // SimpleExpression := Term {AdditiveOperator Term}
* // Term := Factor {MultiplicativeOperator Factor}
* // Factor := Number | Letter | "(" Expression ")"| "!" Factor
* // Number := Digit{Digit}
* // Identifier := Letter {(Letter|Digit)}
* // Declaration := Ident {"," Ident} ":" Type
* // VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
* 
* // AdditiveOperator := "+" | "-" | "||"
* // MultiplicativeOperator := "*" | "/" | "%" | "&&"
* // RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
* // Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
* // Letter := "a"|...|"z"
* // Types := "INTEGER" | "BOOLEAN" | "CHAR" | "DOUBLE"
